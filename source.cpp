#include <iostream>
#include <fstream> 
#include <sstream> 
#include <cmath> 
#include <string>
#include <vector>
#include <unordered_map>

using namespace std;
class Minterm {
private:
	vector<char> bits;
	bool checkRange;
	char f;

public:
	Minterm() {
		this->bits = {};
		this->f = '\0';
	}
	Minterm(vector<char> bits, char f) {
		this->bits = bits;
		this->f = f;
	}
	bool null(void) {return bits.size() == 0;}
	int size(void) {return (int)bits.size();}
	int getOnesAmount(void) {
		int res = 0;
		for (int i = 0; i < bits.size(); i++) {
			if (bits[i] == '1')res++;
		}
		return res;
	}
	friend ostream& operator<< (ostream& ostrm, const Minterm& minterm) {
		for (int i = 0; i < minterm.bits.size(); i++) ostrm << minterm.bits[i];
		ostrm << " ";
		ostrm << minterm.f << endl;
		return ostrm;
	}
	void setMinterm(vector<char>& bits, const char& f) {
		this->bits = bits;
		this->f = f;
	}
	bool rangeCheck(int i) {
		return !(i >= bits.size() && i < 0);
	}
	char& operator[] (int i) {
		if (!rangeCheck(i)) cout << "error[" << i << "]";
		return bits[i];
	}
	char& operator() (void) {return f;}
	friend int operator- (const Minterm& left, const Minterm& right) {
		if (left.bits.size() != right.bits.size())return -1;
		int different = 0;
		for (int i = 0; i < left.bits.size(); i++) {
			if(i%2==0)if (left.bits[i] != right.bits[i]) different++;
		}
		for (int j = 0; j < left.bits.size(); j++) {
			if (j % 2 == 1)if (left.bits[j] != right.bits[j]) different++;
		}
		return different;
	}
	Minterm combine(const Minterm& right) {
		Minterm res;
		if (bits.size() != right.bits.size())return res;

		vector<char> temp;
		for (int i = 0; i < bits.size(); i++) {
			if (bits[i] != right.bits[i]) {
				temp.push_back('-');
			}
			else temp.push_back(bits[i]);
		}

		res.setMinterm(temp, '1');
		return res;
	}
	bool compare(const Minterm& right) {
		if (bits.size() != right.bits.size())return false;

		for (int i = 0; i < bits.size(); i++) {
			if (i % 2 == 0) {
				if (bits[i] != right.bits[i]) {
					if (bits[i] != '-')return false;
				}
			}
		}

		for (int j = 0; j < bits.size(); j++) {
			if (j % 2 == 1) {
				if (bits[j] != right.bits[j]) {
					if (bits[j] != '-')return false;
				}
			}
		}
		return true;
	}
	int literals(void) {
		int literals = 0;
		for (int i = 0; i < bits.size(); i++) {
			if(i%2==0)if (bits[i] == '0' || bits[i] == '1') literals++;
		}
		for (int j = 0; j < bits.size(); j++) {
			if (j % 2 == 1)if (bits[j] == '0' || bits[j] == '1') literals++;
		}
		return literals;
	}
};
//functions
vector<Minterm> Minimize(vector<Minterm>, int, int); // minimize the implicants
vector<Minterm> duplicate(vector<Minterm>); //check for duplicate
vector<Minterm> find_prime_implicant(vector<Minterm>, vector<Minterm>); // find prime implicant
vector<Minterm> PetrickMethod(vector<Minterm>, vector<Minterm>, vector<Minterm>, unordered_map<int, bool>); 
//do PetrickMethod to find possible product terms

int main(int argc, char* argv[]) {
	if (argc != 3) cout << ">$ ./mini input.pla output.pla" << endl;
	else {
	//if(1){
		//ifstream infile("case1.pla");
		//string outfile_path = "outfile.pla";
		ifstream infile(argv[1]);
		string outfile_path = argv[2]; 
		vector<Minterm> product_terms;
		vector<int> unsatisfied_minterms;
		string str;
		int ins = 0;
		int outs = 0;
		int size = 0;
		vector<char> inputVar;
		vector<char> outputVar;
		vector<Minterm> minterms;
		vector<Minterm> minimized_minterms;
		vector<Minterm> temp_table;

		while (infile >> str) {
			if (str == ".i") {
				infile >> ins;
				size = (int)pow(2, ins);
				temp_table.resize(size);
				for (int j = 0; j < size; j++) {
					vector<char> temp_vec;
					for (int i = 0; i < ins; i++) {
						temp_vec.push_back('0');
					}
					Minterm temp(temp_vec, '0');
					temp_table[j] = temp;
				}

				for (int j = 0; j < size; j++) {
					int temp = j, var_sequence = ins - 1;
					while (temp > 0) {
						if (temp % 2 == 1)temp_table[j][var_sequence] = '1';
						temp = (temp - temp % 2) / 2;
						var_sequence--;
					}
					if (temp == 1)temp_table[j][0] = '1';
				}
				minterms = temp_table;
				inputVar.resize(ins);

			}
			else if (str == ".o") {
				infile >> outs;
				outputVar.resize(outs);
			}
			else if (str == ".ilb") {
				char inCH;
				for (int i = 0; i < ins; i++) {
					infile >> inCH;
					inputVar.at(i) = inCH;
				}
			}
			else if (str == ".ob") {
				char outCH;
				for (int i = 0; i < outs; i++) {
					infile >> outCH;
					outputVar.at(i) = outCH;
				}
			}
			else if (str == ".p") {
				int line;
				infile >> line;

				string input;
				while (line--) {
					vector<bool> m(size);
					for (int i = 0; i < size; i++)m[i] = true;
					infile >> input;
					int var_sequence = 0;
					for (int i = 0; i < input.length(); i++) {
						switch (input[i]) {

						}
						if (input[i] == '0') {
							for (int j = 0; j < size; j++) {
								if (minterms[j][var_sequence] != '0') m[j] = false;
							}
							var_sequence++;
						}
						else if (input[i] == '1') {
							for (int j = 0; j < size; j++) {
								if (minterms[j][var_sequence] != '1') m[j] = false;
							}
							var_sequence++;
						}
						else if (input[i] == '-') {
							var_sequence++;
							continue;
						}
					}
					char input_ch;
					infile >> input_ch;
					for (int j = 0; j < size; j++) {
						if (m[j] == 1)minterms[j]() = input_ch;
					}
				}

				////print table
				//for (int j = 0; j < size; j++) {
				//	cout << minterms[j] ;
				//}cout << endl << endl;

			}
			else if (str == ".e") {
				int Number_of_ones = ins + 1;
				minimized_minterms = Minimize(minterms, ins, Number_of_ones);
				minimized_minterms = duplicate(minimized_minterms);
				minimized_minterms = find_prime_implicant(minterms, minimized_minterms);

				break;
			}
		}

		ofstream outfile(outfile_path, ofstream::out);

		outfile << ".i ";
		outfile << inputVar.size() << endl;
		outfile << ".o ";
		outfile << outputVar.size() << endl;

		outfile << ".ilb ";
		for (int i = 0; i < inputVar.size(); i++) {
			if (i != inputVar.size() - 1) outfile << inputVar.at(i) << " ";
			else outfile << inputVar[i] << endl;
		}

		outfile << ".ob ";
		for (int i = 0; i < outputVar.size(); i++) {
			if (i != outputVar.size() - 1) outfile << outputVar.at(i) << " ";
			else outfile << outputVar[i] << endl;
		}

		outfile << ".p ";
		outfile << minimized_minterms.size() << endl;
		for (int i = 0; i < minimized_minterms.size(); i++) {
			outfile << minimized_minterms[i];
		}

		outfile << ".e";
		outfile << endl;
		outfile << endl;

		int literals = 0;
		for (int i = 0; i < minimized_minterms.size(); i++) {
			if (i % 2 == 0)literals = literals + minimized_minterms[i].literals();
		}

		for (int j = 0; j < minimized_minterms.size(); j++) {
			if (j % 2 == 1)literals = literals + minimized_minterms[j].literals();
		}

		outfile << "###  Total number of terms";
		outfile << "(mini): ";
		outfile << minimized_minterms.size() << endl;
		outfile << "###  Total number of literals: ";
		outfile << literals << endl;

		outfile.close();
		cout << outfile_path <<"...done" << endl;
	}

	return 0;
}

vector<Minterm> Minimize(vector<Minterm> minterms, int ins, int Number_of_ones) {
	bool isMinimized = false; //check for minimize process
	vector<vector<Minterm>> minterm_table;
	vector<Minterm> minimized_minterms;//vector for minimized_minterms

	//insert ones_table
	for (int k = 0; k < Number_of_ones; k++) {
		vector<Minterm> temp;
		for (int j = 0; j < minterms.size(); j++) {
			if ((minterms[j]() == '1' || minterms[j]() == '-')
				&& minterms[j].getOnesAmount() == k) {
				temp.push_back(minterms[j]);
			}
		}
		minterm_table.push_back(temp);
	}

	////print table
	//for (int j = 0; j < minterm_table.size(); j++) {
	//	cout << j << ":" << endl;
	//	for (int i = 0; i < minterm_table[j].size(); i++) {
	//		cout << minterm_table[j][i];
	//	}
	//}cout << endl << endl;

	for (int k = 0; k < Number_of_ones - 1; k++) {

		for (int s = 0; s < minterm_table[k].size(); s++) {
			for (int t = 0; t < minterm_table[k + 1].size(); t++) {
				if (minterm_table[k][s] - minterm_table[k + 1][t] == 1) {
					minimized_minterms.push_back(minterm_table[k][s].combine(minterm_table[k + 1][t]));
					isMinimized = true;
				}
			}
		}
	}

	////print table
	//for (int i = 0; i < minimized_minterms.size(); i++) {
	//	cout << i << ':' << minimized_minterms[i];
	//}cout << endl << endl;

	//if minimization hav been done, then  do recursion with Number_of_ones - 1
	if (isMinimized)return Minimize(minimized_minterms, ins, Number_of_ones - 1);
	else return minterms;
}

vector<Minterm> duplicate(vector<Minterm> minimized_minterms) {
	vector<Minterm> res;
	for (int i = 0; i < minimized_minterms.size(); i++) {
		if (res.size() >= 1) {
			bool duplicate = false;
			for (int j = 0; j < res.size(); j++) {
				if (res[j] - minimized_minterms[i] == 0) {
					duplicate = true;
					break;
				}
			}
			if (!duplicate)res.push_back(minimized_minterms[i]);
		}
		else res.push_back(minimized_minterms[i]);
	}

	////print table
	//for (int i = 0; i < res.size(); i++) {
	//	cout << i << ':' << res[i];
	//}cout << endl << endl;

	return res;
}

vector<Minterm> find_prime_implicant(vector<Minterm> minterms, vector<Minterm> minimized_minterms) {
	//unordered_map for saving lefted minterms
	unordered_map<int, bool> unordered_map;

	//initialize prime table
	vector<vector<bool>> prime_table(minimized_minterms.size(), vector<bool>(minterms.size()));
	for (int j = 0; j < minimized_minterms.size(); j++) {
		for (int i = 0; i < minterms.size(); i++) {
			unordered_map.insert({ i, false });
			prime_table[j][i] = false;
		}
	}

	//find prime number with prime table
	for (int i = 0; i < minterms.size(); i++) {
		if (minterms[i]() == '1') {
			for (int j = 0; j < minimized_minterms.size(); j++) {
				if (minimized_minterms[j].compare(minterms[i])) {
					prime_table[j][i] = true;
					unordered_map[i] = true;
				}
			}
		}
	}

	////print prime table
	//for (int j = 0; j < minimized_minterms.size(); j++) {
	//	for (int i = 0; i < minterms.size(); i++) {
	//		cout<<prime_table[j][i];
	//	}
	//	cout << endl;
	//}

	vector<Minterm> prime_implicant;
	for (int i = 0; i < minterms.size(); i++) {
		if (minterms[i]() == '1') {
			int count = 0;
			for (int j = 0; j < minimized_minterms.size(); j++) {
				if (prime_table[j][i])count++;
				if (count > 1)break;
			}
			if (count == 1) {
				for (int j = 0; j < minimized_minterms.size(); j++) {
					if (prime_table[j][i]) {
						prime_implicant.push_back(minimized_minterms[j]);
						for (int s = 0; s < minterms.size(); s++) {
							if (prime_table[j][s])unordered_map[s] = false;
						}
					}
				}
			}
		}
	}

	////print prime_implicant
	//for (int i = 0; i < prime_implicant.size(); i++) {
	//	cout << prime_implicant[i];
	//}

	////print unordered_map
	//for (const auto it : unordered_map) {
	//	cout << it.first << ":" << it.second << endl;
	//}

	//check for duplicate
	prime_implicant = duplicate(prime_implicant);

	vector<Minterm> lefted_implicant;
	for (int i = 0; i < minimized_minterms.size(); i++) {
		bool check = false;
		for (int j = 0; j < prime_implicant.size(); j++) {
			if (minimized_minterms[i].compare(prime_implicant[j])) check = true;
		}
		if (!check)lefted_implicant.push_back(minimized_minterms[i]);
	}

	lefted_implicant = duplicate(lefted_implicant);

	//print lefted_implicant
	//for (int i = 0; i < lefted_implicant.size(); i++) cout << lefted_implicant[i];

	return PetrickMethod(minterms, lefted_implicant, prime_implicant, unordered_map);
}

vector<Minterm> PetrickMethod(vector<Minterm> minterms, vector<Minterm> lefted_implicant, vector<Minterm> prime_implicant, unordered_map<int, bool> unordered_map) {
	vector<Minterm> product_terms;
	vector<int> unsatisfied_minterms;

	//put prime implicant into answer
	for (int i = 0; i < prime_implicant.size(); i++) {
		product_terms.push_back(prime_implicant[i]);
	}

	for (auto it : unordered_map) {
		if (it.second == true)unsatisfied_minterms.push_back(it.first);
	}

	//initialize Petrick table
	vector<vector<bool>> Petrick_table(lefted_implicant.size(), vector<bool>(minterms.size()));
	for (int j = 0; j < lefted_implicant.size(); j++) {
		for (int i = 0; i < minterms.size(); i++) {
			Petrick_table[j][i] = false;
		}
	}

	//find prime number with Petrick table
	for (int i = 0; i < minterms.size(); i++) {
		if (unordered_map[i]) {
			for (int j = 0; j < lefted_implicant.size(); j++) {
				if (lefted_implicant[j].compare(minterms[i]))Petrick_table[j][i] = true;
			}
		}
	}

	////print Petrick table
	//for (int j = 0; j < lefted_implicant.size(); j++) {
	//	for (int i = 0; i < minterms.size(); i++) {
	//		cout<< Petrick_table[j][i];
	//	}
	//	cout << endl;
	//}

	for (int i = 0; i < unsatisfied_minterms.size(); i++) {
		if (unordered_map[unsatisfied_minterms[i]]) {
			for (int j = 0; j < lefted_implicant.size(); j++) {
				if (Petrick_table[j][unsatisfied_minterms[i]]) {
					product_terms.push_back(lefted_implicant[j]);
					unordered_map[unsatisfied_minterms[i]] = false;
					for (int s = 0; s < minterms.size(); s++) {
						if (Petrick_table[j][s])unordered_map[s] = false;
					}
					break;
				}
			}
		}
	}

	//print product_terms
	/*for (int i = 0; i < product_terms.size(); i++) {
		cout << product_terms[i];
	}*/

	product_terms = duplicate(product_terms);

	return product_terms;
}


