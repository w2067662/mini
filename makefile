#-------------definition------------

CC = g++
T_IN = input
T_OUT = output
S_IN = case
S_OUT = case_output
run = ./mini


#--------------commands-------------

all: compile  test

compile: source.cpp
	$(CC) source.cpp -o mini -std=c++11 -lm

test: $(T_IN)1.pla $(T_IN)2.pla $(T_IN)3.pla
	$(run) $(T_IN)1.pla $(T_OUT)1.pla
	$(run) $(T_IN)2.pla $(T_OUT)2.pla
	$(run) $(T_IN)3.pla $(T_OUT)3.pla

clean_all:
	rm $(T_OUT)1.pla $(T_OUT)2.pla $(T_OUT)3.pla
clean_mini:
	rm mini

#--------------do samples-------------

sample: compile  cases

cases: $(S_IN)1.pla $(S_IN)2.pla $(S_IN)3.pla
	$(run) $(S_IN)1.pla $(S_OUT)1.pla
	$(run) $(S_IN)2.pla $(S_OUT)2.pla
	$(run) $(S_IN)3.pla $(S_OUT)3.pla

clean_sample:
	rm $(S_OUT)1.pla $(S_OUT)2.pla $(S_OUT)3.pla
