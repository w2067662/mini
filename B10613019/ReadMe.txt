#you can simply use makefile commands to do all the instructions
Makefile commands:
>$ make all     # read input.pla, and do exact-boolean-minimization,
		# then output output.pla
>$ make sample   # do exact-boolean-minimization with sample cases


>$ make clean_all   # remove test output files
>$ make clean_sample   # remove sample output files


#Explanations
use the following command to create "mini" file:
>$ g++ main.cpp -o mini -std=c++11 -lm


execute mini file to read .PLA file and output .PLA file:
>$ ./mini input1.pla output1.pla

