//****************************************************************
// Editor: B10613019 ChengChingRu
// Latest Update: 2021.11.03
// Program: main.cpp for Exact Boolean Mnimization
// Project: Digital Logic Design Project 3 – Exact Boolean Mnimization
//****************************************************************

#include <iostream>
#include <fstream> //for input and output file
#include <sstream> 
#include <cmath>  //for pow()
#include <string>
#include <vector>
#include <map>

using namespace std;

//class for minterm
class Minterm {
private:
	vector<char> bits;
	char f;

public:
	Minterm() {
		this->bits = {};
		this->f = '\0';
	}

	Minterm(vector<char> bits, char f) {
		this->bits = bits;
		this->f = f;
	}

	//check if null
	bool null(void) {
		return bits.size() == 0;
	}

	//get size
	int size(void) {
		return (int)bits.size();
	}

	//count for ones
	int getOnesAmount(void) {
		int res = 0;
		for (int i = 0; i < bits.size(); i++) {
			if (bits[i] == '1')res++;
		}
		return res;
	}

	//friend operator<< overload
	friend ostream& operator<< (ostream& ostrm, const Minterm& minterm) {
		for (int i = 0; i < minterm.bits.size(); i++) {
			ostrm << minterm.bits[i];
		}
		ostrm << " " << minterm.f << endl;
		return ostrm;
	}

	//set minterm
	void setMinterm(vector<char>& bits, const char& f) {
		this->bits = bits;
		this->f = f;
	}

	//operator[]
	char& operator[] (int i) {
		if (i >= bits.size() && i < 0) cout << "error:operator[" << i << "] out of range!!";
		return bits[i];
	}

	char& operator() (void) {
		return f;
	}
	
	//compare for Minterms, and return different bits amount
	friend int operator- (const Minterm& left, const Minterm& right) {
		if (left.bits.size() != right.bits.size())return -1;

		int different = 0;
		for (int i = 0; i < left.bits.size(); i++) {
			if (left.bits[i] != right.bits[i]) different++;
		}
		return different;
	}

	//1010
	//1011
	//101-
	Minterm combine (const Minterm& right) {
		Minterm res;
		if (bits.size() != right.bits.size())return res;

		vector<char> temp;
		for (int i = 0; i < bits.size(); i++) {
			if (bits[i] != right.bits[i]) {
				temp.push_back('-');
			}
			else temp.push_back(bits[i]);
		}

		res.setMinterm(temp, '1');
		return res;
	}

	//compare two minterm are the same or not
	bool compare(const Minterm& right) {
		if (bits.size() != right.bits.size())return false;

		for (int i = 0; i < bits.size(); i++) {
			if (bits[i] != right.bits[i]) {
				if(bits[i]!='-')return false;
			}
		}
		return true;
	}

	int literals(void) {
		int literals = 0;
		for (int i = 0; i < bits.size(); i++) {
			if (bits[i] == '0' || bits[i] == '1') literals++;
		}
		return literals;
	}
};


//function define
void EB_minimize(ifstream&, string); // do EB_minimize
void Print(string, vector<Minterm>, char*, int, char*, int); //print .PLA file content
void Output(string, vector<Minterm>, char*, int, char*, int); //output .PLA file
vector<Minterm> Minimize(vector<Minterm>, int, int); // minimize the implicants
vector<Minterm> duplicate(vector<Minterm>); //check for duplicate
vector<Minterm> find_prime_implicant(vector<Minterm>, vector<Minterm>); // find prime implicant
vector<Minterm> PetrickMethod(vector<Minterm>, vector<Minterm>, vector<Minterm>, map<int, bool>); //do PetrickMethod to find possible product terms


//main function 
//>$ ./mini input.pla output.pla
int main(int argc, char* argv[]) {
	//argv[1]: infile
	//argv[2]: outfile
	if (argc == 3) {
		ifstream infile(argv[1]);//read .PLA file
		string outfile_path = argv[2]; //get outfile path

		if (!infile) {
			cout << "input file error!!" << endl;
			return -1;
		}
		else {
			EB_minimize(infile, outfile_path);
		}
	}
	else { //catch command error
		cout << "error command, please use:" << endl;
		cout << ">$ ./mini input.pla output.pla" << endl;
		return -1;
	}

	////test on visual studio
	//ifstream infile("input1.pla");
	//EB_minimize(infile, "outfile.pla");

	return 0;
}

void EB_minimize(ifstream& infile, string outfile) {
	string str;
	int inVar_amount = 0, outVar_amount = 0;
	char* ilb = NULL;
	char* ob = NULL;
	vector<Minterm> minterms;
	vector<Minterm> minimized_minterms;

	while (infile >> str) {

		if (str == ".i") { // get input variable amount
			infile >> inVar_amount;	//number of input variables.

			//resize table spaces
			vector<Minterm> temp_table((int)pow(2, inVar_amount));
			for (int j = 0; j < (int)pow(2, inVar_amount); j++) {
				vector<char> temp_vec;
				for (int i = 0;i < inVar_amount; i++) {
					temp_vec.push_back('0');
				}
				Minterm temp(temp_vec, '0');
				temp_table[j] = temp;
			}

			for (int j = 0; j < (int)pow(2, inVar_amount); j++) {
				// temp for minterm table column, var_sequence for minterm table row(last var)
				int temp = j, var_sequence = inVar_amount - 1;
				while (temp > 0) {
					if (temp % 2 == 1)temp_table[j][var_sequence] = '1';
					temp = (temp - temp % 2) / 2;
					var_sequence--;
				}
				if (temp == 1)temp_table[j][0] = '1';
			}

			//initialize minterm table
			minterms = temp_table;

			///print table
			/*for (int j = 0; j < (int)pow(2, inVar_amount); j++) {
				cout << minterms[j];
			}cout << endl << endl;*/

			//char[] for input variables
			ilb = new char[inVar_amount];

		}
		else if (str == ".o") { //get out put variable amount
			infile >> outVar_amount;
			ob = new char[outVar_amount];
		}
		else if (str == ".ilb") { //get input variables
			for (int i = 0; i < inVar_amount; i++) {
				infile >> ilb[i];
			}
		}
		else if (str == ".ob") { //get output variables
			for (int i = 0; i < outVar_amount; i++) {
				infile >> ob[i];
			}
		}
		else if (str == ".p") { //get following input  ex:-10-1 1
			int line;
			infile >> line;

			string input;
			while (line--) {
				//initiliza m (from 0 to 15)
				vector<bool> m((int)pow(2, inVar_amount));
				for (int i = 0; i < (int)pow(2, inVar_amount); i++)m[i] = true;

				//insert boolean into table
				infile >> input;

				int var_sequence = 0;
				for (int i = 0; i < input.length(); i++) {
					if (input[i] == '0') {
						for (int j = 0; j < (int)pow(2, inVar_amount); j++) {
							if (minterms[j][var_sequence] != '0') m[j] = false;
						}
						var_sequence++;
					}
					else if (input[i] == '1') {
						for (int j = 0; j < (int)pow(2, inVar_amount); j++) {
							if (minterms[j][var_sequence] != '1') m[j] = false;
						}
						var_sequence++;
					}
					else if (input[i] == '-') {
						var_sequence++;
						continue;
					}
				}

				//insert last bit
				char input_ch;
				infile >> input_ch;

				//insert f by using operator()
				for (int j = 0; j < (int)pow(2, inVar_amount); j++) {
					if (m[j] == 1)minterms[j]() = input_ch;
				}
			}

			////print table
			//for (int j = 0; j < (int)pow(2, inVar_amount); j++) {
			//	cout << minterms[j] ;
			//}cout << endl << endl;

		}
		else if (str == ".e") {
			//minimize prime implicant chart
			int Number_of_ones = inVar_amount+1;
			minimized_minterms = Minimize(minterms, inVar_amount, Number_of_ones);

			//check for duplicate
			minimized_minterms = duplicate(minimized_minterms);

			//find prime implicant
			minimized_minterms = find_prime_implicant(minterms, minimized_minterms);

			//print and output minimized .PLA file
			Print(outfile, minimized_minterms, ilb, inVar_amount, ob, outVar_amount);
			Output(outfile, minimized_minterms, ilb, inVar_amount, ob, outVar_amount);

			break;
		}
	}
}

vector<Minterm> Minimize(vector<Minterm> minterms, int inVar_amount, int Number_of_ones) {
	bool isMinimized = false; //check for minimize process
	vector<vector<Minterm>> minterm_table; 
	vector<Minterm> minimized_minterms;//vector for minimized_minterms

	//insert ones_table
	for (int k = 0; k < Number_of_ones; k++) {
		vector<Minterm> temp;
		for (int j = 0; j < minterms.size(); j++) {
			if ((minterms[j]() == '1' || minterms[j]() == '-')
				&& minterms[j].getOnesAmount() == k) {
				temp.push_back(minterms[j]);
			}
		}
		minterm_table.push_back(temp);
	}

	////print table
	//for (int j = 0; j < minterm_table.size(); j++) {
	//	cout << j << ":" << endl;
	//	for (int i = 0; i < minterm_table[j].size(); i++) {
	//		cout << minterm_table[j][i];
	//	}
	//}cout << endl << endl;

	for (int k = 0; k< Number_of_ones - 1; k++) {

		for (int s = 0; s < minterm_table[k].size(); s++) {
			for (int t = 0; t < minterm_table[k + 1].size(); t++) {
				if (minterm_table[k][s] - minterm_table[k + 1][t] == 1) { 
					minimized_minterms.push_back(minterm_table[k][s].combine(minterm_table[k + 1][t]));
					isMinimized = true;
				}
			}
		}
	}

	////print table
	//for (int i = 0; i < minimized_minterms.size(); i++) {
	//	cout << i << ':' << minimized_minterms[i];
	//}cout << endl << endl;

	//if minimization hav been done, then  do recursion with Number_of_ones - 1
	if (isMinimized)return Minimize(minimized_minterms, inVar_amount, Number_of_ones - 1);
	else return minterms;
}

vector<Minterm> duplicate(vector<Minterm> minimized_minterms) {
	vector<Minterm> res;
	for (int i = 0; i < minimized_minterms.size(); i++) {
		if (res.size() >= 1) {
			bool duplicate = false;
			for (int j = 0; j < res.size(); j++) {
				if (res[j] - minimized_minterms[i] == 0) {
					duplicate = true;
					break;
				}
			}
			if(!duplicate)res.push_back(minimized_minterms[i]);
		}
		else res.push_back(minimized_minterms[i]);
	}

	////print table
	//for (int i = 0; i < res.size(); i++) {
	//	cout << i << ':' << res[i];
	//}cout << endl << endl;

	return res;
}

vector<Minterm> find_prime_implicant(vector<Minterm> minterms, vector<Minterm> minimized_minterms) {
	//map for saving lefted minterms
	map<int, bool> map;

	//initialize prime table
	vector<vector<bool>> prime_table(minimized_minterms.size(), vector<bool>(minterms.size()));
	for (int j = 0; j < minimized_minterms.size(); j++) {
		for (int i = 0; i < minterms.size(); i++) {
			map.insert({ i, false });
			prime_table[j][i] = false;
		}
	}

	//find prime number with prime table
	for (int i = 0; i < minterms.size(); i++) {
		if (minterms[i]() == '1') {
			for (int j = 0; j < minimized_minterms.size(); j++) {
				if (minimized_minterms[j].compare(minterms[i])) {
					prime_table[j][i] = true;
					map[i]=true;
				}
			}
		}
	}

	////print prime table
	//for (int j = 0; j < minimized_minterms.size(); j++) {
	//	for (int i = 0; i < minterms.size(); i++) {
	//		cout<<prime_table[j][i];
	//	}
	//	cout << endl;
	//}

	vector<Minterm> prime_implicant;
	for (int i = 0; i < minterms.size(); i++) {
		if (minterms[i]() == '1') {
			int count = 0;
			for (int j = 0; j < minimized_minterms.size(); j++) {
				if (prime_table[j][i])count++;
				if (count > 1)break;
			}
			if (count == 1) {
				for (int j = 0; j < minimized_minterms.size(); j++) {
					if (prime_table[j][i]) {
						prime_implicant.push_back(minimized_minterms[j]);
						for (int s = 0; s < minterms.size(); s++) {
							if (prime_table[j][s])map[s] = false;
						}
					}
				}
			}
		}
	}

	////print prime_implicant
	//for (int i = 0; i < prime_implicant.size(); i++) {
	//	cout << prime_implicant[i];
	//}

	////print map
	//for (const auto it : map) {
	//	cout << it.first << ":" << it.second << endl;
	//}

	//check for duplicate
	prime_implicant = duplicate(prime_implicant);

	vector<Minterm> lefted_implicant;
	for (int i = 0; i < minimized_minterms.size(); i++) {
		bool check = false;
		for (int j = 0; j < prime_implicant.size(); j++) {
			if (minimized_minterms[i].compare(prime_implicant[j])) check = true;
		}
		if(!check)lefted_implicant.push_back(minimized_minterms[i]);
	}

	lefted_implicant = duplicate(lefted_implicant);
	
	//print lefted_implicant
	//for (int i = 0; i < lefted_implicant.size(); i++) cout << lefted_implicant[i];

	return PetrickMethod(minterms, lefted_implicant, prime_implicant, map);
}

vector<Minterm> PetrickMethod(vector<Minterm> minterms, vector<Minterm> lefted_implicant, vector<Minterm> prime_implicant, map<int, bool> map) {
	vector<Minterm> product_terms;
	vector<int> unsatisfied_minterms;

	//put prime implicant into answer
	for (int i = 0; i < prime_implicant.size(); i++) {
		product_terms.push_back(prime_implicant[i]);
	}

	for (auto it : map) {
		if (it.second == true)unsatisfied_minterms.push_back(it.first);
	}

	//initialize Petrick table
	vector<vector<bool>> Petrick_table(lefted_implicant.size(), vector<bool>(minterms.size()));
	for (int j = 0; j < lefted_implicant.size(); j++) {
		for (int i = 0; i < minterms.size(); i++) {
			Petrick_table[j][i] = false;
		}
	}

	//find prime number with Petrick table
	for (int i = 0; i < minterms.size(); i++) {
		if (map[i]) {
			for (int j = 0; j < lefted_implicant.size(); j++) {
				if (lefted_implicant[j].compare(minterms[i]))Petrick_table[j][i] = true;
			}
		}
	}

	////print Petrick table
	//for (int j = 0; j < lefted_implicant.size(); j++) {
	//	for (int i = 0; i < minterms.size(); i++) {
	//		cout<< Petrick_table[j][i];
	//	}
	//	cout << endl;
	//}
	
	for (int i = 0; i < unsatisfied_minterms.size(); i++) {
		if (map[unsatisfied_minterms[i]]) {
			for (int j = 0; j < lefted_implicant.size(); j++) {
				if (Petrick_table[j][unsatisfied_minterms[i]]) {
					product_terms.push_back(lefted_implicant[j]);
					map[unsatisfied_minterms[i]] = false;
					for (int s = 0; s < minterms.size(); s++) {
						if (Petrick_table[j][s])map[s] = false;
					}
					break;
				}
			}
		}
	}

	//print product_terms
	/*for (int i = 0; i < product_terms.size(); i++) {
		cout << product_terms[i];
	}*/

	product_terms = duplicate(product_terms);

	return product_terms;
}

void Print(string name, vector<Minterm> minimized_minterms, char* ilb, int ilb_size, char* ob, int ob_size) {

	cout << ".i " << ilb_size << endl;
	cout << ".o " << ob_size << endl;

	cout << ".ilb ";
	for (int i = 0; i < ilb_size; i++) {
		if (i != ilb_size - 1) {
			cout << ilb[i] << " ";
		}
		else cout << ilb[i] << endl;
	}

	cout << ".ob ";
	for (int i = 0; i < ob_size; i++) {
		if (i != ob_size - 1) {
			cout << ob[i] << " ";
		}
		else cout << ob[i] << endl;
	}

	cout << ".p " << minimized_minterms.size() << endl;
	for (int i = 0; i < minimized_minterms.size(); i++) {
		cout << minimized_minterms[i];
	}

	cout << ".e" << endl << endl;

	int literals = 0;
	for (int i = 0; i < minimized_minterms.size(); i++) {
		literals += minimized_minterms[i].literals();
	}

	cout << "#Total number of terms: " << minimized_minterms.size() << endl;
	cout << "#Total number of literals: " << literals << endl;

	return;
}

void Output(string fileName, vector<Minterm> minimized_minterms, char* ilb, int ilb_size, char* ob, int ob_size) {

	ofstream outfile(fileName, ofstream::out);

	outfile << ".i " << ilb_size << endl;
	outfile << ".o " << ob_size << endl;

	outfile << ".ilb ";
	for (int i = 0; i < ilb_size; i++) {
		if (i != ilb_size - 1) {
			outfile << ilb[i] << " ";
		}
		else outfile << ilb[i] << endl;
	}

	outfile << ".ob ";
	for (int i = 0; i < ob_size; i++) {
		if (i != ob_size - 1) {
			outfile << ob[i] << " ";
		}
		else outfile << ob[i] << endl;
	}

	outfile << ".p " << minimized_minterms.size() << endl;
	for (int i = 0; i < minimized_minterms.size(); i++) {
		outfile << minimized_minterms[i];
	}

	outfile << ".e" << endl << endl;

	int literals = 0;
	for (int i = 0; i < minimized_minterms.size(); i++) {
		literals += minimized_minterms[i].literals();
	}

	outfile << "#Total number of terms: " << minimized_minterms.size() << endl;
	outfile << "#Total number of literals: " << literals << endl;

	outfile.close();
	return;
}
